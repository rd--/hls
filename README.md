hls
---

[haskell](http://haskell.org) lindenmayer systems

> Aristid Lindenmayer, "Mathematical models for cellular interaction
> in development." _J. Theoret. Biology_, 18:280—315, 1968.

© [rohan drape](http://rohandrape.net/),
  2006-2025,
  [gpl](http://gnu.org/copyleft/)
