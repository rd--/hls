all:
	echo "hls"

mk-cmd:
	echo "hls - NIL"

clean:
	rm -fR dist

push-all:
	r.gitlab-push.sh hls

push-tags:
	r.gitlab-push.sh hosc --tags

indent:
	fourmolu -i LSystem

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns LSystem
