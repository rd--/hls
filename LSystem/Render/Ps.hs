-- | Postscript renderer for 'LSystem's.
module LSystem.Render.Ps where

import Music.Theory.Geometry.Functions {- hmt-base -}
import Music.Theory.Math {- hmt-base -}

import qualified Data.Cg.Minus.Core as Cg {- hcg-minus -}
import qualified Data.Cg.Minus.Types as Cg {- hcg-minus -}

import qualified Graphics.Ps as Ps {- hps -}

import LSystem.LSystem
import LSystem.Turtle

-- | Given initial 'ta' (in degrees), 'ls' and 'lls' values render /i/ steps of an 'LSystem'.
renderL :: (LSystem, Double, Double) -> Int -> Double -> [Cg.Ln R]
renderL (l, ta0, lls0) i ll0 =
  let a = Turtle (degrees_to_radians ta0) Cg.pt_origin (pi / 2) ll0 lls0 []
  in render [] (\ll'' p1 p2 -> Cg.Ln p1 p2 : ll'') (expand l i) a

-- | Draw output of 'renderL' normalised, scaled to /m/ and translated by /o/.
draw :: R -> R -> [Cg.Ln R] -> Ps.Image
draw o m l =
  let g = Ps.greyGs 0.5
      p = Ps.renderLines_jn (Cg.lns_normalise m l)
  in Ps.translate o o (Ps.Stroke g p)
