-- | Lindenmayer system definition, expander and renderer.
module LSystem.LSystem where

import qualified Data.Map as M

import Data.Cg.Minus {- hcg-minus -}
import Music.Theory.Math {- hmt-base -}

import qualified LSystem.Turtle as T

-- | Element of 'Axiom'.
type Element = Char

-- | An axiom (sequence of 'Elements').
type Axiom = [Element]

-- | A 'M.Map' from 'Element's to 'Axiom's.
type Rules = M.Map Element Axiom

-- | An 'LSystem' is an 'Axiom' and a set of 'Rules'.
data LSystem = LSystem Axiom Rules deriving (Eq, Show)

{- | L-System constructor.

>>> lSystem "F+F+F" [('F',"F-F+F")]
LSystem "F+F+F" (fromList [('F',"F-F+F")])
-}
lSystem :: Axiom -> [(Element, [Element])] -> LSystem
lSystem a rs = LSystem a (M.fromList rs)

-- | Rule lookup.
getRule :: Rules -> Element -> [Element]
getRule rs c = M.findWithDefault [c] c rs

-- | Rule application.
applyRule :: [Element] -> Rules -> [Element]
applyRule a rs = concatMap (getRule rs) a

{- | /n/ iterations of the specified 'LSystem'.

>>> let f p q n = expand (lSystem p q) n
>>> f "A" [('A',"AB"),('B',"A")] 5
"ABAABABAABAAB"

>>> f "0" [('1',"11"),('0',"1[0]0")] 3
"1111[11[1[0]0]1[0]0]11[1[0]0]1[0]0"

>>> f "A" [('A',"ABA"),('B',"BBB")] 3
"ABABBBABABBBBBBBBBABABBBABA"

>>> f "F" [('F',"F+F-F-F+F")] 2
"F+F-F-F+F+F+F-F-F+F-F+F-F-F+F-F+F-F-F+F+F+F-F-F+F"

>>> f "F+F+F" [('F',"F-F+F")] 1
"F-F+F+F-F+F+F-F+F"
-}
expand :: LSystem -> Int -> [Element]
expand (LSystem a rs) n =
  case n of
    0 -> a
    _ -> expand (LSystem (applyRule a rs) rs) (n - 1)

-- * Turtle interpreter

-- | State transformer 'Turtle' commands.
stateT :: Element -> T.Turtle -> T.Turtle
stateT e =
  case e of
    '+' -> T.turnRight
    '-' -> T.turnLeft
    '|' -> T.turnBack
    '>' -> T.incrLine
    '<' -> T.decrLine
    '[' -> T.push
    ']' -> T.pop
    'f' -> T.forward
    _ -> id

-- | Operational 'Turtle' commands.
cmd :: (T.Turtle -> b -> (T.Turtle, b)) -> Element -> T.Turtle -> b -> (T.Turtle, b)
cmd f e t i =
  case e of
    'F' -> f t i
    _ -> (stateT e t, i)

-- | Fold over an expanded L-system using standard turtle commands.
render :: b -> (b -> Pt R -> Pt R -> b) -> [Element] -> T.Turtle -> b
render i f l t =
  let g (u, j) c = cmd (T.stepTurtle f) c u j
  in snd (foldl g (t, i) l)
