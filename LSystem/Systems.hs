{- | Various 'LSystem's.

For 'l0' through 'lB' see <http://paulbourke.net/fractals/lsys/>.
For 'lC' see <http://en.wikipedia.org/wiki/Penrose_tiling>.
For 'lD' see <http://hackage.haskell.org/package/nymphaea>.
For 'p0' etc. see <http://algorithmicbotany.org/papers/#abop>.
-}
module LSystem.Systems where

import Music.Theory.Geometry.Functions {- hmt-base -}

import qualified Data.Cg.Minus.Types as Cg {- hcg-minus -}

import qualified Graphics.Ps as Ps {- hps -}

import LSystem.LSystem
import LSystem.Render.Ps

-- * 'LSystem' definitions

l0, l1, l2, l3, l4, l5, l6, l7, l8, l9, lA, lB :: LSystem
l0 = lSystem "F+F+F+F" [('F', "F+F-F-FF+F+F-F")]
l1 = lSystem "F+F+F+F" [('F', "FF+F-F+F+FF")]
l2 =
  lSystem
    "X"
    [ ('F', "FF")
    , ('X', "F-[[X]+X]+F[+FX]-X")
    ]
l3 =
  lSystem
    "a"
    [ ('F', ">F<")
    , ('a', "F[+x]Fb")
    , ('b', "F[-y]Fa")
    , ('x', "a")
    , ('y', "b")
    ]
l4 =
  lSystem
    "Y"
    [ ('X', "X[-FFF][+FFF]FX")
    , ('Y', "YFX[+Y][-Y]")
    ]
l5 = lSystem "F" [('F', "FF+[+F-F-F]-[-F+F+F]")]
l6 =
  lSystem
    "X"
    [ ('F', "FF")
    , ('X', "F[+X]F[-X]+X")
    ]
l7 = lSystem "F" [('F', "F[+FF][-FF]F[-F][+F]F")]
l8 =
  lSystem
    "F"
    [ ('F', "FFF-[XY]+[XY]")
    , ('X', "+FY")
    , ('Y', "-FX")
    ]
l9 = lSystem "FX" [('X', ">[-FX]+FX")]
lA =
  lSystem
    "FX"
    [ ('Y', "-FX-Y")
    , ('X', "X+YF+")
    ]
lB = lSystem "F+F+F" [('F', "F-F+F")]

lC :: LSystem
lC =
  lSystem
    "[7]++[7]++[7]++[7]++[7]"
    [ ('6', "8F++9F----7F[-8F----6F]++")
    , ('7', "+8F--9F[---6F--7F]+")
    , ('8', "-6F++7F[+++8F++9F]-")
    , ('9', "--8F++++6F[+9F++++7F]--7F")
    , ('F', "")
    ]

lD :: LSystem
lD = lSystem "F+F+F+F+F+F" [('F', "F-F++F-F")]

p00, p01, p02, p03, p04, p05 :: LSystem
p00 = lSystem "F-F-F-F" [('F', "F-F+F+FF-F-F+F")]
p01 = lSystem "F-F-F-F" [('F', "F+FF-FF-F-F+F+FF-F-F+F+FF+FF-F")]
p02 = lSystem "-F" [('F', "F+F-F-F+F")]
p03 = lSystem "F+F+F+F" [('F', "F+f-FF+F+FF+Ff+FF-f+FF-F-FF-Ff-FFF"), ('f', "ffffff")]
p04 = lSystem "F-F-F-F" [('F', "FF-F-F-F-F-F+F")]
p05 = lSystem "F-F-F-F" [('F', "FF-F-F-F-FF")]

-- * With turning angle (in radians) and line scalar

rad :: Double -> Double
rad = degrees_to_radians

l0d, l1d, l2d, l3d, l4d, l5d, l6d, l7d, l8d, l9d, lAd, lBd, lCd, lDd :: (LSystem, Double, Double)
l0d = (l0, rad 90.0, 1)
l1d = (l1, rad 90.0, 1)
l2d = (l2, rad 22.5, 1)
l3d = (l3, rad 45.0, 1.36)
l4d = (l4, rad 25.7, 1)
l5d = (l5, rad 22.5, 1)
l6d = (l6, rad 20.0, 1)
l7d = (l7, rad 35.0, 1)
l8d = (l8, rad 22.5, 1)
l9d = (l9, rad 40.0, 0.6)
lAd = (lA, rad 90.0, 1)
lBd = (lB, rad 120.0, 1)
lCd = (lC, rad 36.0, 1)
lDd = (lD, rad 60.0, 1)

p00d, p01d, p02d, p03d, p04d, p05d :: (LSystem, Double, Double)
p00d = (p00, rad 90, 1)
p01d = (p01, rad 90, 1)
p02d = (p02, rad 90, 1)
p03d = (p03, rad 90, 1)
p04d = (p04, rad 90, 1)
p05d = (p05, rad 90, 1)

-- * Line sets

l0l, l1l, l2l, l3l, l4l, l5l, l6l, l7l, l8l, l9l, lAl, lBl, lCl, lDl :: [Cg.Ln Double]
l0l = renderL l0d 3 5
l1l = renderL l1d 3 5
l2l = renderL l2d 5 5
l3l = renderL l3d 12 5
l4l = renderL l4d 6 5
l5l = renderL l5d 4 5
l6l = renderL l6d 7 5
l7l = renderL l7d 4 5
l8l = renderL l8d 6 5
l9l = renderL l9d 10 40
lAl = renderL lAd 11 5
lBl = renderL lBd 6 5
lCl = renderL lCd 5 5
lDl = renderL lDd 5 1

p00l_a, p00l_b, p00l_c, p00l_d, p01l, p02l, p03l, p04l, p05l :: [Cg.Ln Double]
p00l_a = renderL p00d 0 1
p00l_b = renderL p00d 1 1
p00l_c = renderL p00d 2 1
p00l_d = renderL p00d 3 1
p01l = renderL p01d 2 1
p02l = renderL p02d 4 1
p03l = renderL p03d 2 1
p04l = renderL p04d 4 1
p05l = renderL p05d 4 1

-- * Collation

{-
> import qualified Data.Cg.Minus.Core as Cg {- hcg-minus -}
> map (Cg.wn_from_extent . Cg.lns_minmax) systems_l
-}
systems_l :: [[Cg.Ln Double]]
systems_l =
  [ l0l
  , l1l
  , l2l
  , l3l
  , l4l
  , l5l
  , l6l
  , l7l
  , l8l
  , l9l
  , lAl
  , lBl
  , lCl
  , lDl
  , p00l_a
  , p00l_b
  , p00l_c
  , p00l_d
  , p01l
  , p02l
  , p03l
  , p04l
  , p05l
  ]

-- * Ps Images

{-
> import qualified Graphics.Ps.Statistics as Ps
> map Ps.imageStatistics systems_i
-}
systems_i :: [Ps.Image]
systems_i = map (draw 25 750) systems_l

{- | Generate postscript file with drawings of 'l0' through 'lD'.

> systems_ps "/tmp/hls.ps"
> System.Process.system "gv /tmp/hls.ps"
-}
systems_ps :: FilePath -> IO ()
systems_ps fn = Ps.ps fn (Ps.Paper Ps.Mm 285 285) systems_i

-- Local Variables:
-- truncate-lines:t
-- End:
